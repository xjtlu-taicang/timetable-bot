FROM centos:centos7.9.2009
RUN useradd -ms /bin/bash timetablebot
USER timetablebot
COPY ./bin/timetablebot /data/timetablebot
COPY ./timetable-bot /data
WORKDIR /data
ENTRYPOINT ["/data/timetablebot"]
